<?php

/**
 * @file
 * Allow users to create a taxonomy term and use it automatically for content.
 *
 * Management and form functions only required on menu pages.
 */

/**
 * Menu callback and dispatcher.
 *
 * Based on the current status of the user and ownterm, different options are displayed:
 * 1) If the user is not associated with any ownterm tids, then they are given the ability to create one.
 * 2) If the vocabulary is set up for mulitple values, and the user can create an additional term, they are
 *   allowed to review their available blogs.
 * 3) if there is already a term for this user, then we allow them to edit that term.
 *
 */
function _ownterm_user_blog($account) {
  $vid = variable_get('ownterm_vocabulary', FALSE);
  $vocabulary = taxonomy_vocabulary_load($vid);
  $tids = ownterm_tids($account->uid);
  if (empty($tids)) {
    return drupal_get_form('ownterm_term_form', $account);
  }
  elseif (user_access('create additional terms')) {
    return _ownterm_overview($account, $tids);
  }
  else {
    $term = taxonomy_term_load(array_pop($tids));
    return drupal_get_form('ownterm_term_form', $account, $term);
  }
}

/**
 * This is the information that is displayed if a user can create additional terms and is viewing their current terms.
 */
function _ownterm_overview($user, $tids) {
  $output = '<h3>Available Blogs</h3>';
  $terms = array();

  foreach ($tids as $tid) {
    $term = taxonomy_term_load($tid);
    $terms[] = array(
      'title' => $term->name,
      'href' => 'user/' . $user->uid . '/blog/edit/' . $tid,
    );
  }
  $output .= theme('links', array('links' => $terms));

  if (user_access('create additional terms')) {
    $output .= l('Add', 'user/' . $user->uid . '/blog/add');
  }
  return $output;
}

/**
 * This is the term form that allows you to create or edit your blog name and description.
 * These are values that get written to term_data.
 */
function ownterm_term_form($form, &$form_state, $user, $term = array()) {
  $vid = variable_get('ownterm_vocabulary', FALSE);
  $vocabulary = taxonomy_vocabulary_load($vid);
  $form['#term'] = $term;
  $form['#vocabulary'] = (array) $vocabulary;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Blog name'),
    '#default_value' => isset($term->name) ? $term->name : '',
    '#maxlength' => 255,
    '#description' => t('The name of your blog.'),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => !empty($term->description) ? $term->description : '',
    '#description' => t('A description of your blog.'),
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  $form['vid'] = array(
    '#type' => 'value',
    '#value' => $vocabulary->vid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  if (!empty($term->tid)) {
    if (user_access('delete own term')) {
      $form['delete'] = array(
        '#value' => l(t('Delete'), 'user/' . $user->uid . '/blog/delete/' . $term->tid),
        '#weight' => 101,
      );
    }
    $form['tid'] = array(
      '#type' => 'value',
      '#value' => $term->tid,
    );
  }
  $form['#redirect'] = 'user/' . $user->uid;
  return $form;
}

/**
 * Process submitted term add/edit form.
 */
function ownterm_term_form_submit($form, &$form_state) {
  if (empty($form_state['values']['tid'])) {
    $term = new stdClass();
  }
  else {
    $term = taxonomy_term_load($form_state['values']['tid']);
  }
  $term->name = $form_state['values']['name'];
  $term->description = $form_state['values']['description'];
  $term->vid = $form_state['values']['vid'];
  switch (taxonomy_term_save($term)) {
    case SAVED_NEW:
      // Select the term, we need it for associating it to the user.
      // @todo: The term id should be in $form_state['values'] now, we should get it from there.
      // Not sure if this is necessary.
      /*$terms = taxonomy_get_term_by_name($term->name);
      if (count($terms) == 1) {
        $tid = $terms[0]->tid;
      }
      else {
        // More than one term with this name, find the one not associated.
        $tid = _ownterm_unassociated($terms);
      }*/
      if (ownterm_create($form_state['values']['uid'], $term->tid) == SAVED_NEW) {
        drupal_set_message(t('%term created.', array('%term' => $term->name)));
        //watchdog('taxonomy', 'Created new term %term.', array('%term' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/structure/taxonomy/edit/term/' . $form_state['values']['tid']));
        /* // not sure what this does
        $has_role = db_query("SELECT COUNT(*) FROM {users_roles} WHERE uid = :uid AND rid = :rid", array(':uid' => $form_state['values']['uid'], ':rid' => variable_get('ownterm_auto_role', 0)))->fetchField();
        if (variable_get('ownterm_auto_role', 0) && ($has_role == 0)) {
          $id = db_insert('users_roles')
            ->fields(array(
              'uid' => $form_state['values']['uid'],
              'rid' => variable_get('ownterm_auto_role', 0),
            ))
            ->execute();
        }*/
      }
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('%term updated.', array('%term' => $term->name)));
      break;
  }
}

/**
 * Form for deleting association.
 */
function ownterm_term_delete($form, &$form_state, $user, $term) {
  // @todo form element for user?
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => 'term',
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $term['name'],
  );
  $form['tid'] = array(
    '#type' => 'value',
    '#value' => $term['tid'],
  );
  $form['delete'] = array(
    '#type' => 'value',
    '#value' => TRUE,
  );
  return confirm_form($form,
                  t('Are you sure you want to delete %title?',
                  array('%title' => $term['name'])),
                  'user/' . $user->uid,
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Process submitted term confirm form deletion.
 */
function ownterm_term_delete_submit($form, &$form_state) {
  if ($form_state['values']['delete'] === TRUE) {
    taxonomy_term_delete($form_state['values']['tid']);
    taxonomy_check_vocabulary_hierarchy($form['#vocabulary'], $form_state['values']);
    drupal_set_message(t('Deleted %name.', array('%name' => $form_state['values']['name'])));
    watchdog('taxonomy', 'Deleted term %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
    $form_state['redirect'] = 'user/' . $form_state['values']['uid'];
    // Remove association.
    ownterm_delete($form_state['values']['uid'], $form_state['values']['tid']);
  }
}

/**
 * Return tid from terms not associated with users.
 */
function _ownterm_unassociated($terms) {
  $sql = "SELECT uid FROM {ownterm} WHERE tid = :tid";
  foreach ($terms as $term) {
    $data = db_query($sql, array(':tid' => $term->tid))->fetch();
    if ($data === FALSE) {
      // No uid for this term.
      return $term->tid;
    }
  }
  return FALSE;
}
