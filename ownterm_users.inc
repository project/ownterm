<?php

/**
 * @file
 *
 * This file defines the user-management forms for adding people to a blog.
 */

/**
 * Implements hook_form().
 *
 * @param array $form_state
 * @param array $term
 * @return array
 */
function ownterm_users_form($form, &$form_state, $term) {
  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#autocomplete_path' => 'user/autocomplete',
    '#size' => '60',
    '#maxlength' => '60',
  );
  $form['tid'] = array(
    '#type' => 'hidden',
    '#value' => $term['tid'],
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * ownterm_users_form submit handler
 *
 * @param array $form
 * @param array $form_state
 */
function ownterm_users_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Cancel')) {
    drupal_set_message(t("Changes have been canceled"));
    drupal_goto('taxonomy/term/' . $form_state['values']['tid'] . '/users');
  }
  $tid = $form_state['values']['tid'];
  foreach ($_POST as $key => $element) {
    if (preg_match('/\Auid_([0-9]*)/', $key, $matches)) {
      $value = $_POST[$key];
      $uid = $matches[1];
      if ($value != 'delete') {
        $existing = db_fetch_object(db_query("SELECT uid from {ownterm} WHERE tid = :tid AND uid = :uid", array(':tid' => $tid, ':uid' => $uid)));
        if (!$existing->uid) {
          // TODO Please convert this statement to the D7 database API syntax.
          /* db_query("INSERT INTO {ownterm} (tid, uid, status) VALUES(%d, %d, %d)", $tid, $uid, 1) */
          NULL;
        }
      }
      else {
        // TODO Please review the conversion of this statement to the D7 database API syntax.
        /* db_query("DELETE FROM {ownterm} WHERE tid = %d AND uid = %d", $tid, $uid) */
        db_delete('ownterm')
  ->condition('tid', $tid)
  ->condition('uid', $uid)
  ->execute();
      }
    }
  }
  drupal_set_message(t("Changes have been saved"));
}

/**
 * Themes the ownterm_users form to add the jquery goodness.
 *
 * @param array $form Form to theme
 * @return string Rendered HTML.
 */
function theme_ownterm_users_form($variables) {
  $form = $variables['form'];
  drupal_add_js( drupal_get_path('module', 'ownterm') . '/ownterm_users.js');

  $rows = array();
  $rows[] = array(
    array('data' => drupal_render($form['user'])),
    array(
      'data' => '<br /><a href="#" id="adduser">' . t('add user') . '</a>',
      'style' => 'width: 100%;',
    ),
  );

  $output = theme('table', array('header' => array(), 'rows' => $rows));

  $rows = array();
  $header = array(t('Author'), t('Email'), t('Actions'));
  $result = db_query("SELECT tid, uid, status FROM {ownterm} WHERE tid = :tid", array(':tid' => $form['tid']['#value']));
  while ($row = db_fetch_object($result)) {
    // TODO Convert "user_load" to "user_load_multiple" if "$row->uid" is other than a uid.
    // To return a single user object, wrap "user_load_multiple" with "array_shift" or equivalent.
    // Example: array_shift(user_load_multiple(array(), $row->uid))
    $account = user_load($row->uid);
    if ($row->status == 0) {
      $status = t('can\'t comment');
    }
    else {
      $status = t('can comment');
    }
    $rows[] = array(
      'data' => array(l($account->name, 'user/' . $account->uid), l($account->mail, 'mailto:' . $account->mail), '<a href="#" onclick="uidEdit(' . $account->uid . '); return false;">' . t('edit') . '</a> | <a href="#" onclick="uidDelete(' . $account->uid . '); return false;">' . t('delete') . '</a>'),
      'id' => 'uid-' . $account->uid,
    );
  }
  if (count($rows) == 0) {
    $rows[] = array(array(
        'data' => t('No users have been assigned to this blog.'),
        'colspan' => 3,
        'id' => 'norecords',
      ));
  }
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'exceptions')));

  // container div to hold hidden form fields that get dynamically written with javascript
  $output .= '<div id="hidden_fields"></div>';

  unset($form['exceptions']);

  return $output . drupal_render_children($form);
}
