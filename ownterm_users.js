
/*
 * This file contains the jquery goodness for adding and removing users from a blog.
 */
function uidDelete(uid) {
 $("#uid-"+ uid).remove();
 changed($('#exceptions'));
 // if a hidden form field exists for this user set it's value to delete
 if($('#uid_'+ uid).length > 0) {
   $('#uid_'+ uid).val('delete');
 }
 else {
  var hiddenField = '<input type="hidden" name="uid_'+ uid +'" id="uid_'+ uid +'" value="delete" />';
  $('#hidden_fields').prepend(hiddenField);
 }

 if($("#exceptions tr:eq(1) td:eq(1)").html() == null) {
    $('#exceptions').prepend('<tr class="odd"><td id="norecords" colspan="3">No users have been assigned to this blog.</td></tr>');
 }
};
function uidEdit(uid) {
  var answer = confirm("Edit selected user? This will navigate you away from the current page and you will loose any unsaved worked.")
    if (answer){
      window.location = '/user/'+ uid +'/edit';
    }
}
function changed(target) {
  if($("#changed-warning").length == 0) {
    target.after('<div id="changed-warning" class="warning"><span class="warning tabledrag-changed">*</span> ' + Drupal.t("The changes to these users will not be saved until the <em>Save</em> button is clicked.") + '</div>');
  }
}

$(document).ready(function() {
 $("#adduser").click(function() {
   var newUser = function (data) {
     if(data.exception) {
       alert(data.exception);
     }
     else {
      if($('#uid-'+ data.uid).length == 0) {
        content = '<tr id="uid-'+ data.uid +'"><td>'+ data.author +'</td><td>'+ data.email +'</td><td><a href="#" id="uid-edit">edit</a> | <a href="#" id="uid-delete">'+ data.actions +'</a></td></tr>';
        hiddenField = '<input type="hidden" name="uid_'+ data.uid +'" id="uid_'+ data.uid +'" value="'+ data.uid +'" />';
        $('#hidden_fields').prepend(hiddenField);
        $('#exceptions').prepend(content);
        changed($('#exceptions'));
        $("#norecords").remove();
        $("#uid-delete").click(function() {
          uidDelete(data.uid);
          return false;
        });
        $("#uid-edit").click(function() {
          uidEdit(data.uid);

        });
      }
      else {
        alert('That user already exists.');
      }
     }
   }
   if($("#edit-user").val().length == 0) {
     alert('Enter a user to add');
   }
   else {
     $.ajax({
       type: 'POST',
       url: Drupal.settings.basePath + 'taxonomy_blog/adduser',
       dataType: 'json',
       success: newUser,
       data: 'name='+ $("#edit-user").val()
     });
   }
   return false;
 });
});
