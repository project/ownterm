
The Own Term module grew out of the need to create blogs that were more flexible than
the standard blog that comes out of the box with Drupal, particularly:

1. We wanted blogs to have a name different than the user who wrote them.
2. We wanted to include other things than blog nodes in a blog stream, such as polls.
3. We wanted a blog that more than one person could contribute posts to.

After some thought, it occured to us that really, all a blog is is a collection of nodes.
And if you want to collect a bunch of nodes together, then taxonomy seems to be the best
way to start.  They've even got taxonomy out of the box.

When you enable ownterm you need to set up a vocabulary which is set up for ownterm by clicking
on the 'Enable Ownterm' checkbox in the vocabulary's edit page, and this should be an empty
vocabulary to start with.  After that you need to give at least the 'create and edit term'
permission to authenticated users.

Once this is done, any user with that permission, when they go to their account /user page, will
see a tab allowing them to create a blog.  This form, once filled out, will create a new taxonomy
term for them and associate it with them inside of the 'ownterm' table.  Once this is done, any
node that they create that is allowed to take a term from the ownterm vocabulary will automatically
be given their blog term without being displayed.  (The form at base is the blog title and description,
which are the term title and description for term_data.)

You will probably want to use pathauto and go in to automatically create a pretty path for the blogs.

For multi-user blogs, once the blog is created go to the taxonomy/term/%tid/users path to add
other users to this blog.

If a user is set up to contribute to two blogs, then they will get a choice between those terms when they
create a post, but won't be able to choose a term that isn't linked to them in the ownterm table.

For further functionality, look for taxonomy_blog module, coming soon.

This module was written by Ben Jeavons, and further modified by Kevin Bridges and John Fiala.
This module was created with the sponsorship of SportingNews (www.sportingnews.com), and released with
the sponsorship of pingVision (http://pingv.com/).

--------------------------
Permissions:
--------------------------
These are the permissions established by ownterm and what they do.

* administer ownterm:
Allows the user to go to the admin/settings/ownterm form, create a blog for any user,
and to change the blog any post is in.

* create and edit term:
Allows a user to create a blog, and to edit the name and title of their blog.

* create additional terms:
Allows the user to create more than the one blog each user normally gets.

* delete own term:
Allows the user to delete his or her blog.

* manage own term users:
Allows the user to add or remove other people to the blog they have created.

* manage any term users
Allows the user to add or remove other people from any blog.


--------------------------
Hooks Defined by Own Term:
--------------------------

hook_ownterm_create($tid, $uid) {}

This hook is invoked when a term in the ownterm vocabulary is linked to a given user.
Listening for this hook will allow you to respond to new blogs being created.  This hook
is called after the row is written to the ownterm table.

hook_ownterm_delete($tid, $uid) {}

This hook is invoked when a term is unlinked to a given user.
Listening for this hook will allow you to respond to a user being removed from a blog.
This hook is called before the row in ownterm is deleted.
